// Test of simple enum error - name already used

enum State {
        OFF,
        ON,
        OFF
};

void main() {
    enum State state = ON;
    byte* const SCREEN = 0x0400;
    *SCREEN = state;
}

